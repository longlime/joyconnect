(function($) {

	var param = {
		dur: 1000
	};

	var timer;

	function app(block) {
		this.items = block.find(".appearance-list li");
		this.navitems = block.find(".appearance-nav li");

		this.items.find(".appearance-preview").css({opacity: 0});
	}

	app.current = 0;

	var zIndex = 1000;
	app.prototype.show = function(id) {
		// console.log( this.items );
		var that = this;
		var afterShowing = function() {
			that.items.eq(id).siblings().find(".appearance-preview").css({opacity: 0});
		};
		that.items
			.eq(id)
				.css("z-index", zIndex += 10)
				.find("p").animate({opacity: 1}, 500).end()
				.find(".appearance-preview").animate({opacity: 1}, 500, afterShowing).end()
			.siblings()
				.find("p").animate({opacity: 0}).end()
		// that.items.eq(id).css({opacity: 1}).siblings().find("p").animate({opacity: 0}, 500);
		// setTimeout(function() {
		// 	that.items.filter(":not(:eq("+id+"))")
		// 		.addClass("_noanim").css({opacity: 0}).removeClass("_noanim")
		// 		.find("p").css({opacity: 1});
		// }, 500);
		that.navitems.removeClass("_active").eq(id).addClass("_active");
	};

	app.prototype.next = function(e, o, cllb) {
		clearTimeout(timer);
		app.current += 1;

		if (app.current > 8) {
			app.current = 8;
			o.stop();
			window.cansliding = true;
		} else {
			o.show(app.current);
		}

		timer = setTimeout(function() {
			if (cllb) cllb();
		}, param.dur + 400);
	};

	app.prototype.prev = function(e, o, cllb) {
		clearTimeout(timer);
		app.current -= 1;

		if (app.current < 0) {
			app.current = 0;
			o.stop();
			window.cansliding = true;
		} else {
			o.show(app.current);
		}

		timer = setTimeout(function() {
			if (cllb) cllb();
		}, param.dur + 400);
	};

	app.prototype.start = function() {
		var that = this;
		that.show(0);
		var $appearance = $("#appearance");
		$appearance.bind("-next", that.next);
		$appearance.bind("-prev", that.prev);
		$appearance.on("click", ".appearance-nav li", function() {
			app.current = $(this).index();
			that.show(app.current);
		});
		$appearance.find(".appearance-arrow-prev").on("click", function(e) {
			app.current -= 1;
			if (app.current < 0) app.current = 0;

			that.show(app.current);
		});
		$appearance.find(".appearance-arrow-next").on("click", function(e) {
			app.current += 1;
			if (app.current > 8) app.current = 8;

			that.show(app.current);
		});
	};

	app.prototype.stop = function() {
		var $appearance = $("#appearance");
		$appearance.unbind("-next", this.next);
		$appearance.unbind("-prev", this.prev);
		$appearance.off("click");
		$appearance.find(".appearance-arrow-prev").off("click");
		$appearance.find(".appearance-arrow-next").off("click");
		// console.log( "as" );
		this.show(0);
		app.current = 0;
		window.stoptype = 0;
	};

	window.CBottomslider = app;

})(jQuery);
