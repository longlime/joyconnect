var addVendorEventListener = function (e, h, g, f) {
    if (typeof (addEventListener) == "function") {
        if (h.match(/^webkit/i)) {
            h = h.replace(/^webkit/i, "")
        } else {
            if (h.match(/^moz/i)) {
                h = h.replace(/^moz/i, "")
            } else {
                if (h.match(/^ms/i)) {
                    h = h.replace(/^ms/i, "")
                } else {
                    if (h.match(/^o/i)) {
                        h = h.replace(/^o/i, "")
                    } else {
                        h = h.charAt(0).toUpperCase() + h.slice(1)
                    }
                }
            }
        } if (/WebKit/i.test(navigator.userAgent)) {
            e.addEventListener("webkit" + h, g, f)
        } else {
            if (/Opera/i.test(navigator.userAgent)) {
                e.addEventListener("O" + h, g, f)
            } else {
                if (/Gecko/i.test(navigator.userAgent)) {
                    e.addEventListener(h.toLowerCase(), g, f)
                } else {
                    h = h.charAt(0).toLowerCase() + h.slice(1);
                    return e.addEventListener(h, g, f)
                }
            }
        }
    }
}

var getVendorPrefix = function(name) {
	var suffix = name.charAt(0).toUpperCase() + name.slice(1);
	var arrayOfPrefixes = [name, "ms" + suffix, "Moz" + suffix, "Webkit" + suffix, "O" + suffix];
	var tmp = document.createElement("div");

	for (var i = 0; i < arrayOfPrefixes.length; ++i) {
		if (arrayOfPrefixes[i] in tmp.style) {
			return arrayOfPrefixes[i];
		}
	}
};

var getWithCSSPrefix = function(name) {
	var prefix = getVendorPrefix(name).toLowerCase().replace(name, "");
	if (prefix != "") {
		prefix = "-" + prefix + "-";
	}
	return prefix + name;
};

var setCSSValue = function(target, key, value) {
	var needPrefixArr = ["transform"];
	for (var i = 0; i < needPrefixArr.length; i++) {
		value = value.replace(needPrefixArr[i], getWithCSSPrefix(needPrefixArr[i]));
	}

	target.style[getVendorPrefix(key)] = value;
};

var getCSSValue = function(target, key) {
    return target.style[getVendorPrefix(key)];
};

var removeCSSValue = function(target, key) {
	target.style.removeProperty(getVendorPrefix(key));
};
