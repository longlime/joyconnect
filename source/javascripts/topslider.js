(function($) {

	var param = {
		dur: 1000
	};

	var timer;

	function app(slider) {
		this.slider = slider;
		this.item = slider.find(".slider-item");
		this.shadow = slider.find(".slider-shadow");
		this.navitems = slider.find(".slider-nav li");
	}

	var positions = {
		0: [
			{top: 364, left: -361, opacity: 0},
			{top: 287, left:   21, opacity: 1},
			{top: 210, left:  403, opacity: 1},
			{top: 131, left:  781, opacity: 1},
			{top:  52, left: 1159, opacity: 0},
		],
		1: [
			{top: 287, left:   21, opacity: 1},
			{top: 210, left:  403, opacity: 1},
			{top: 131, left:  781, opacity: 1},
			{top:  52, left: 1159, opacity: 0},
			{top: 364, left: -361, opacity: 0},
		],
		2: [
			{top: 210, left:  403, opacity: 1},
			{top: 131, left:  781, opacity: 1},
			{top:  52, left: 1159, opacity: 0},
			{top: 364, left: -361, opacity: 0},
			{top: 287, left:   21, opacity: 1},
		],
		3: [
			{top: 131, left:  781, opacity: 1},
			{top:  52, left: 1159, opacity: 0},
			{top: 364, left: -361, opacity: 0},
			{top: 287, left:   21, opacity: 1},
			{top: 210, left:  403, opacity: 1},
		],
		4: [
			{top:  52, left: 1159, opacity: 0},
			{top: 364, left: -361, opacity: 0},
			{top: 287, left:   21, opacity: 1},
			{top: 210, left:  403, opacity: 1},
			{top: 131, left:  781, opacity: 1},
		]
	};

	var positions_shadow = {};

	for (var id in positions) {
		var arr = positions[id];
		positions_shadow[id] = [];
		for (var i = 0; i < arr.length; i++) {
			positions_shadow[id][i] = {};
			positions_shadow[id][i].top = positions[id][i].top + 15;
			positions_shadow[id][i].left = positions[id][i].left - 10;
			positions_shadow[id][i].opacity = positions[id][i].opacity;
		}
	}

	app.current = 0;

	app.prototype.showSlide = function(id) {
		for (var i = 0; i < 5; i++) {
			this.item.eq(i).css(positions[id][i]);
			this.shadow.eq(i).css(positions_shadow[id][i]);
		}
		this.navitems.removeClass("_active").eq(id).addClass("_active");
	};

	app.prototype.next = function(e, o, cllb) {
		clearTimeout(timer);

		app.current += 1;
		if (app.current > 4) {
			o.stop();
			window.cansliding = true;
		} else {
			o.showSlide(app.current);
		}

		timer = setTimeout(function() {
			if (cllb) cllb();
		}, param.dur + 400);
	};

	app.prototype.prev = function(e, o, cllb) {
		clearTimeout(timer);

		app.current -= 1;
		if (app.current < 0) {
			app.current = 0;
			// o.stop();
			// window.cansliding = true;
		} else {
			o.showSlide(app.current);
		}

		timer = setTimeout(function() {
			if (cllb) cllb();
		}, param.dur + 400);
	};

	app.prototype.start = function() {
		var that = this;
		$("#slider").bind("-next", that.next);
		$("#slider").bind("-prev", that.prev);
		$("#slider").on("click", ".slider-nav li", function() {
			app.current = $(this).index();
			that.showSlide(app.current);
		});
	};

	app.prototype.stop = function() {
		$("#slider").unbind("-next", this.next);
		$("#slider").unbind("-prev", this.prev);
		$("#slider").off("click");
		// console.log( $("#slider").find(".slider-item").eq(0) );
		this.showSlide(0);
		app.current = 0;
		window.stoptype = 0;
	};

	window.CTopslider = app;

})(jQuery);
