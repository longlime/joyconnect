//= require jquery
//= require flowplayer
//= require jquery.swipe.vertical
//= require topslider
//= require bottomslider
//= require vendor_prefixes
//= require animation

$(function() {
	var Bottomslider = new CBottomslider( $("#appearance") );
	Bottomslider.start();
	Bottomslider.show(0);
	var Topslider = new CTopslider( $("#slider") );
	Topslider.start();

	var fp = flowplayer();

	var slider = document.getElementById('swipe_slider');

	window.mySwipe = Swipe(slider, {
		// stopPropagation: true,
		continuous: false,
		speed: 500
	});

	window.mobilecheck = function() {
		var check = false;
		(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
		return check;
	}

	window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return false;};

	$(".progress-nav").on("click", "li", function() {
		mySwipe.slide($(this).index(), 500);
	});

	var $slider = $(slider);
	var $body = $("body");
	var $contactMail = $(".contact-mail");

	var changeScaling = function() {
		var bodyscale = $body.width() / 1400;
		var bodyleft = ($body.width()-1400)/2;

		$slider.find(".inner").each(function() {
			var $this = $(this);
			var parent = $this.parent().parent();

			if (parent.is("#video")) {
				setCSSValue($this.get(0), "transform", "scale("+bodyscale.toFixed(3)+", "+bodyscale.toFixed(3)+")");
			}
			if (parent.is("#appearance")) {
				if ($body.hasClass("-portrait")) {
					var x = $(window).width()/840;
					x = x.toFixed(3);
					setCSSValue($this.get(0), "transform", "scale("+x+", "+x+")");
				} else {
					setCSSValue($this.get(0), "transform", "scale("+bodyscale.toFixed(3)+", "+bodyscale.toFixed(3)+")");
				}
			}
			if (parent.is("#header")) {
				if ($body.hasClass("-portrait")) {
					var x = $(window).width()/960;
					x = x.toFixed(3);
					setCSSValue($this.get(0), "transform", "scale("+x+", "+x+")");
				} else {
					setCSSValue($this.get(0), "transform", "scale("+bodyscale.toFixed(3)+", "+bodyscale.toFixed(3)+")");
				}
			}
			if (parent.is("#slider")) {
				if ($body.hasClass("-portrait")) {
					var documentHeight = $(window).height();
					var x = Math.min( documentHeight / 700, $(window).width()/960 );
					x = x.toFixed(3);
					setCSSValue($this.get(0), "transform", "scale("+x+", "+x+")");
					var blockHeight = $this.find(".preview-list").height();
					$(".-mobile.-portrait").find("#preview .inner").css({top: ( documentHeight-blockHeight)/2});
				} else {
					setCSSValue($this.get(0), "transform", "scale("+bodyscale.toFixed(3)+", "+bodyscale.toFixed(3)+")");
				}
			}
			if (parent.is("#preview")) {
				if ($body.hasClass("-portrait")) {
					var documentHeight = $(window).height();
					var blockHeight = $this.find(".preview-list").height();
					var blockWidth = $this.find(".preview-list").width();
					var x = Math.min( documentHeight/(blockHeight+40), $slider.width()/(blockWidth+20) );
					x = x.toFixed(3);
					setCSSValue($this.get(0), "transform", "scale("+x+", "+x+")");
					$(".-mobile.-portrait").find("#preview .inner").css({top: ( documentHeight-blockHeight)/2});
				} else {
					setCSSValue($this.get(0), "transform", "scale("+bodyscale.toFixed(3)+", "+bodyscale.toFixed(3)+")");
				}
			}
			if (parent.is("#contact")) {
				if ($body.hasClass("-portrait")) {
					var x = Math.min( ($(window).height()-420)/($(".contact-form").height()+40 + 34) , $(window).width()/($(".contact-form").width()+40) );
					var v = 0;
					if (x < 1) x = 1;
					setCSSValue($this.get(0), "transform", "scale("+x.toFixed(3)+", "+x.toFixed(3)+")");
					setCSSValue($contactMail.get(0), "transform", "scale(.5, .5)");
					$contactMail.css("top", v);
				} else {
					setCSSValue($contactMail.get(0), "transform", "scale(1, 1)");
					$contactMail.css("top", 60);
					setCSSValue($this.get(0), "transform", "scale("+bodyscale.toFixed(3)+", "+bodyscale.toFixed(3)+")");
				}
			}

			$this.css("left", bodyleft);
		});
	};

	var resizeSlides = function(what) {
		var documentHeight = $slider.height();

		$slider.find("> div > div").each(function() {
			var $this = $(this);
			var $full = $this.find(".slide._full");

			var top = (documentHeight - 700)/2;

			if ($full.is("#slider")) {
				top = (documentHeight - 800)/2;
			}
			if ($full.is("#video")) {
				top = 0;
			}

			$full.find(".inner").css({top: top});
			// $(".-mobile").find("#contact .inner").css({top: (documentHeight-$(".contact-form").height() - 200)/2 + 140});
			$(".-mobile.-portrait").find("#contact .inner").css({top: 200});
		});

		changeScaling();
	};

	var setOrientation_landscape = function() {
		$body.removeClass("-portrait");
	};

	var setOrientation_portrait = function() {
		$body.addClass("-portrait");
	};

	var detectOrientation = function() {
		switch (window.orientation) {
		case -90:
		case 90:
			setOrientation_landscape();
			break;
		default:
			setOrientation_portrait();
		}
	};

	var orientationchange = function() {
		detectOrientation();
		resizeSlides();
		changeScaling();
	};

	$(window)
		.on("resize", resizeSlides)
		.on("orientationchange", orientationchange);

	function orientation_changed () {
	    // if ( is_portrait() ) {
	    //     setOrientation_portrait();
	    // } else if ( is_landscape() ) {
	    //     setOrientation_landscape();
	    // }
	    detectOrientation();
	    resizeSlides();
		changeScaling();
	    // clearTimeout(window.t);
	    // delete window.t;
	}

	// window.t = undefined;
	// window.onorientationchange = function (event) {
	//     window.t = setTimeout('orientation_changed();', 250);
	// }

	// function is_landscape() {
	//     var r = window.innerHeight < window.innerWidth;
	//     return r;
	// }

	// function is_portrait() {
	//     var r = window.innerHeight > window.innerWidth;
	//     return r;
	// }

	$body.addClass("-mobile");
	orientationchange();

	var changeNav = function(index) {
		$(".progress-nav").find("li").removeClass("active").eq(index).addClass("active");
	};

	var showNextSlide = function(i) {
		if (Anim[i]) Anim[i](fp);
	};

	var showPrevSlide = function(i) {
		var anim = Anim[i+1 + "_"];
		if (anim) anim(fp);
		anim = Anim["_"+i];
		if (anim) anim(fp);
	};

	// Anim.before();
	showNextSlide(0);

	var prevPosition = 0;
	$slider.bind("swipe:slided", function(e, position) {
		if (position > prevPosition) {
			showNextSlide(position);
		} else if (position < prevPosition) {
			showPrevSlide(position);
		}

		if (position != prevPosition) {
			changeNav(position);
			prevPosition = position;
		}
	});

	$(".slider-link.appearance-arrow-next").on("click", function(e) {
		Topslider.next(e, Topslider);
	});
	$(".slider-link.appearance-arrow-prev").on("click", function(e) {
		Topslider.prev(e, Topslider);
	});
});
