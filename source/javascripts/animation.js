(function( $, window ) {

	var durations = {
		newslide: 1000,
		item: 500
	}

	$(function() {

		$.extend($.easing, {
			easeOutElastic: function (x, t, b, c, d) {
				var s=1.70158;var p=0;var a=c;
				if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
				if (a < Math.abs(c)) { a=c; var s=p/4; }
				else var s = p/(2*Math.PI) * Math.asin (c/a);
				return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
			}
		});

		var timer2_1, timer2_2;
		var anim = {
			dur: durations,

			0: function() {
				var $headerLogo = $(".header-logo");
				var headerLogoTop = $headerLogo.css("top");
				$headerLogo.css({top: - $headerLogo.height()});
				$headerLogo.animate({top: headerLogoTop}, anim.dur.item);

				var $headerSocial = $(".header-social");
				setCSSValue($headerSocial.get(0), "transform", "scale(0, 0)");
				$headerSocial.animate({ping: 1}, {
					duration: anim.dur.item * 2,
					easing: "easeOutElastic",
					step: function(x) {
						setCSSValue($headerSocial.get(0), "transform", "scale("+x.toFixed(3)+", "+x.toFixed(3)+")");
					}
				});

				var $headerPresskit = $(".header-presskit");
				var headerPresskitTop = $headerPresskit.css("top");
				$headerPresskit.css({top: - $headerPresskit.height()});
				$headerPresskit.animate({top: headerPresskitTop}, anim.dur.item);

				var $sliderInstall = $(".slider-install");
				$sliderInstall.animate({ping: 1}, {
					duration: anim.dur.item * 2,
					easing: "easeOutElastic",
					step: function(x) {
						setCSSValue($sliderInstall.get(0), "transform", "scale("+x.toFixed(3)+", "+x.toFixed(3)+")");
					}
				});
			},

			1: function() {
				$(".preview-borders").animate({opacity: 1}, anim.dur.item);
				$("#preview p").animate({opacity: 1}, anim.dur.item);

					$("#preview .-item-1 img, #preview .-item-4 img").animate({opacity: 1}, anim.dur.item);

				timer2_1 = setTimeout(function() {
					$("#preview .-item-2 img, #preview .-item-5 img").animate({opacity: 1}, anim.dur.item);
				}, anim.dur.item);

				timer2_2 = setTimeout(function() {
					$("#preview .-item-3 img, #preview .-item-6 img").animate({opacity: 1}, anim.dur.item);
				}, anim.dur.item * 2);
			},

			"1_": function() {
				clearTimeout(timer2_1);
				clearTimeout(timer2_2);
				$(".preview-borders, #preview p, #preview img").stop();//.css({opacity: 0});

					$("#preview .-item-3 img, #preview .-item-6 img").animate({opacity: 0}, anim.dur.item);

				timer2_1 = setTimeout(function() {
					$("#preview .-item-2 img, #preview .-item-5 img").animate({opacity: 0}, anim.dur.item);
				}, anim.dur.item);

				timer2_2 = setTimeout(function() {
					$("#preview .-item-1 img, #preview .-item-4 img").animate({opacity: 0}, anim.dur.item);
					$(".preview-borders").animate({opacity: 0}, anim.dur.item);
					$("#preview p").animate({opacity: 0}, anim.dur.item);
				}, anim.dur.item * 2);
			},

			2: function(fp) {
				$("#video").animate({opacity: 1}, anim.dur.item);
				$(".slider-skip").css({opacity: 0});
				fp.play();
			},

			"2_": function(fp) {
				$("#video").animate({opacity: 0}, anim.dur.item);
				fp.pause();
			},

			3: function(fp) {
				fp.pause();
			},

			"3_": function(fp) {
				// $("#video").animate({opacity: 0}, anim.dur.item);
				// fp.pause();
			},

			"_3": function(fp) {
				$("#download a").animate({bottom: -141}, anim.dur.item);
				$("#contact form").animate({opacity: 0}, anim.dur.item);
				setCSSValue($(".contact-mail").get(0), "transform", "scale(0, 0)");
			},

			4: function(fp) {
				$("#download a").animate({bottom: 0}, anim.dur.item);
				$("#contact form").animate({opacity: 1}, anim.dur.item);
				$(".slider-skip").css({opacity: 0});

				var $contactMail = $(".contact-mail");
				var max = $("body").is(".-mobile.-portrait") ? 0.5 : 1;
				$contactMail.css({ping: 0}).stop().animate({ping: max}, {
					duration: anim.dur.item * 2,
					easing: "easeOutElastic",
					step: function(x) {
						setCSSValue($contactMail.get(0), "transform", "scale("+x.toFixed(3)+", "+x.toFixed(3)+")");
					}
				});
			},

			// "_4": function() {
			// 	$("#download a").animate({bottom: -141}, anim.dur.item);
			// 	$("#contact form").animate({opacity: 0}, anim.dur.item);
			// 	setCSSValue($(".contact-mail").get(0), "transform", "scale(0, 0)");
			// },

			// 5: function() {
			// 	$("#download a").animate({bottom: 0}, anim.dur.item);
			// 	$("#contact form").animate({opacity: 1}, anim.dur.item);

			// 	var $contactMail = $(".contact-mail");
			// 	$contactMail.css({ping: 0}).animate({ping: 1}, {
			// 		duration: anim.dur.item * 2,
			// 		easing: "easeOutElastic",
			// 		step: function(x) {
			// 			setCSSValue($contactMail.get(0), "transform", "scale("+x.toFixed(3)+", "+x.toFixed(3)+")");
			// 		}
			// 	});
			// },

			// "5_": function() {
			// 	$("#download a").animate({bottom: -141}, anim.dur.item);
			// 	$("#contact form").animate({opacity: 0}, anim.dur.item);
			// 	setCSSValue($(".contact-mail").get(0), "transform", "scale(0, 0)");
			// },

			// "6_": function() {
			// 	$("#download a").animate({bottom: -141}, anim.dur.item);
			// 	$("#contact form").animate({opacity: 0}, anim.dur.item);
			// 	setCSSValue($(".contact-mail").get(0), "transform", "scale(0, 0)");
			// },

			before: function() {
				setCSSValue($(".slider-install").get(0), "transform", "scale(0, 0)");
				$(".preview-borders, #preview p, #preview img").css({opacity: 0});
				$("#video").css({opacity: 0});
				$("#download a").css({bottom: -141});
				$("#contact form").animate({opacity: 0}, anim.dur.item);
				setCSSValue($(".contact-mail").get(0), "transform", "scale(0, 0)");

				// anim[0]();
			}
		};

		anim[5] = anim[4];
		anim["5_"] = anim["4_"];

		anim.before();

		window.Anim = anim;

	});

})( jQuery, window );
