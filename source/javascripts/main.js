(function( $, window ) {

	var $window = $(window);
	var windowHeight = $window.height();

	$(function() {

		var param = {
			newslideDuration: Anim.dur.newslide
		};

		var fp = flowplayer();
		var Topslider = new CTopslider( $("#slider") );
		var Bottomslider = new CBottomslider( $("#appearance") );

		var timer_slide;
		var timer_item;

		var maxcoord = -700 - 3*windowHeight;
		var coords = [0, -windowHeight, -2*windowHeight, -3*windowHeight, -4*windowHeight, maxcoord];
		var position = 0;
		var prevPosition = 0;

		loadData = function() {
			windowHeight = $("html").height();
			maxcoord = -4*windowHeight;
			coords = [0, -windowHeight, -2*windowHeight, -3*windowHeight, -4*windowHeight];
		};

		loadData();

		var notPrevPosition = function(pos) {
			return pos != prevPosition;
		};

		var $main = $("#main");
		var _main = $main.get(0);

		var $progress = $(".progress-nav");

		window.cansliding = true;
		window.stoptype = 0;

		var changeProgressnav = function(id) {
			if (id == 5) id = 4;
			$progress.find(".active").removeClass("active").end().find("li:eq("+id+")").addClass("active");
		};

		var showSlide = function(id, isanim, isnext) {
			var top = coords[id];
			// console.log( top + ' '+maxcoord );

			if (top <= 0 && top >= maxcoord && cansliding) {
				// console.log( id );
				clearTimeout(timer_slide);
				removeDetecting();
				$(".slider-skip").css({opacity: 1});

				if (isnext && Anim[id]) {
					timer_item = setTimeout(function() {
						Anim[id](fp);
					}, param.newslideDuration - 200);

				} else if (!isnext && Anim[(id+1)+"_"]) {
					Anim[(id+1)+"_"](fp);
				}

				if (!isnext && Anim["_"+id]) {
					Anim["_"+id](fp);
				}

				if (id == 0) {
					cansliding = false;
					stoptype = 1;
					Topslider.start();
				}

				if (id == 3) {
					cansliding = false;
					stoptype = 2;
					Bottomslider.start();
				}

				// if (isnext && id == 1) {
				// 	cansliding = false;
				// 	stoptype = 1;
				// 	Topslider.start();
				// }
				// if (isnext && id == 4) {
				// 	cansliding = false;
				// 	stoptype = 2;
				// 	Bottomslider.start();
				// }

				if (isanim) {
					setCSSValue(_main, "transition", "transform "+(param.newslideDuration/1000)+"s ease");
				}

				changeProgressnav(id);

				setCSSValue(_main, "transform", "translate3d(0px, " + top + "px, 0px)");

				timer_slide = setTimeout(function() {
					removeCSSValue(_main, "transition");
					addDetecting();
				}, 1400);
			}
		};

		var wheelDelta = function(next) {
			// console.log( stoptype );
			if (cansliding) {
				position = next ? position += 1 : position -= 1;

				if (position < 0) position = 0;
				else if (position > coords.length - 1) position = coords.length - 1;

				if (notPrevPosition(position)) {
					showSlide(position, true, position > prevPosition);
					prevPosition = position;
				}
			} else {
				if (stoptype == 1) {
					removeDetecting();
					// console.log( CTopslider.current );
					if (next) {
						if (CTopslider.current == 4) {
							position = 1;
							prevPosition = position;
							Topslider.stop();
							cansliding = true;
							showSlide(position, true, true);
						} else {
							$("#slider").trigger("-next", [Topslider, addDetecting]);
						}
					} else {
						$("#slider").trigger("-prev", [Topslider, addDetecting]);
					}
				}
				if (stoptype == 2) {
					removeDetecting();
					// console.log( CBottomslider.current );
					if (next) {
						if (CBottomslider.current == 8) {
							// alert(position)
							// position = windowHeight < 700 ? 4 : 5;
							position = 4;
							prevPosition = position;
							Bottomslider.stop();
							cansliding = true;
							showSlide(position, true, true);
						} else {
							$("#appearance").trigger("-next", [Bottomslider, addDetecting]);
						}
					} else {
						if (CBottomslider.current == 0) {
							position = 2;
							prevPosition = position;
							Bottomslider.stop();
							cansliding = true;
							showSlide(position, true, true);
						} else {
							$("#appearance").trigger("-prev", [Bottomslider, addDetecting]);
						}
					}
				}
			}
			//  else if (stoptype == 1) {
			// 	removeDetecting();
			// 	// console.log( CTopslider.current );
			// 	if (next) {
			// 		if (CTopslider.current == 4) {
			// 			position = 2;
			// 			prevPosition = 2;
			// 			Topslider.stop();
			// 			cansliding = true;
			// 			showSlide(2, true, true);
			// 		} else {
			// 			$("#slider").trigger("-next", [Topslider, addDetecting]);
			// 		}
			// 	} else {
			// 		// console.log( CTopslider.current );
			// 		if (CTopslider.current == 0) {
			// 			position = 0;
			// 			prevPosition = 0;
			// 			Topslider.stop();
			// 			cansliding = true;
			// 			showSlide(0, true, false);
			// 		} else {
			// 			$("#slider").trigger("-prev", [Topslider, addDetecting]);
			// 		}
			// 	}
			// } else if (stoptype == 2) {
			// 	// console.log( "as" );
			// 	removeDetecting();
			// 	// if (next) {
			// 	// 	$("#appearance").trigger("-next", [Bottomslider, addDetecting]);
			// 	// }
			// 	// // console.log( CTopslider.current );
			// 	if (next) {
			// 		if (CBottomslider.current == 8) {
			// 			position = 5;
			// 			prevPosition = 5;
			// 			Bottomslider.stop();
			// 			cansliding = true;
			// 			showSlide(5, true, true);
			// 		} else {
			// 			$("#appearance").trigger("-next", [Bottomslider, addDetecting]);
			// 		}
			// 	} else {
			// 		// console.log( CTopslider.current );
			// 		if (CBottomslider.current == 0) {
			// 			position = 3;
			// 			prevPosition = 3;
			// 			Bottomslider.stop();
			// 			cansliding = true;
			// 			showSlide(3, true, false);
			// 		} else {
			// 			$("#appearance").trigger("-prev", [Bottomslider, addDetecting]);
			// 		}
			// 	}
			// }
		};

		var detectWheeling = function(e) {
			var delta = -e.deltaY || e.detail || e.wheelDelta;

			if (delta) {
				wheelDelta(delta > 0);
			}

			return false;
		};

		var detectKey = function(e) {
			if (e.keyCode == 38 /* Arrow UP */) {
				e.preventDefault();
				wheelDelta(false);

			} else if (e.keyCode == 40 /* Arrow DOWN */) {
				e.preventDefault();
				wheelDelta(true);
			}
		};

		var addDetecting = function() {
			// if ("onwheel" in document) {
			// 	$window.on("wheel", detectWheeling);
			// } else {
			$window.on("mousewheel", detectWheeling);
			// }
			$window.on("keydown", detectKey);
		};

		var removeDetecting = function() {
			// $window.off("wheel", detectWheeling);
			$window.off("mousewheel", detectWheeling);
			$window.off("keydown", detectKey);
		};

		window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return false;};

		addDetecting();
		showSlide(0, false, true);

		var $fullSlides = $(".slide._full");

		var checkDimensions = function() {
			// var x = windowHeight < 700 ? (windowHeight/700).toFixed(2) : 1;
			// var top = x < 1 ? (windowHeight-700)/2 : "0";

			$fullSlides.each(function() {
				var elem = this.children[0];
				var x = windowHeight < 700 ? (windowHeight/700).toFixed(2) : 1;
				// console.log( windowHeight );
				var top = (windowHeight-700)/2;

				if ($(this).is("#slider")) {
					x = windowHeight-100 < 700 ? ((windowHeight-100)/700).toFixed(2) : 1;
					top = (windowHeight-800)/2;
					this.style.height = (windowHeight - 100) + "px";
				}
				if ($(this).is("#video")) {
					x = 1;
					top = (windowHeight - $("#video .content").height())/2;
				}
				if ($(this).is("#contact")) {
					x = windowHeight-200 < 500 ? ((windowHeight-200)/500).toFixed(2) : 1;
					top = (windowHeight-700)/2;
					// top = top < 200 ? 0 : top;
					this.style.height = (windowHeight - 200) + "px";
				}
				// if ($(this).is("#contact")) {
				// 	x = windowHeight-200 < 700 ? ((windowHeight-200)/700).toFixed(2) : 1;
				// 	top = 0;
				// 	this.style.height = (windowHeight - 200) + "px";
				// }
				elem.style.top = top + "px";
				setCSSValue(elem, "transform", "scale("+x+","+x+")");
			});
		};

		checkDimensions();

		var pageResized = function() {
			loadData();

			checkDimensions();
			showSlide(position, false, true);
		};

		$window.on("resize", pageResized);

		$progress.on("click", "li:not(.active)", function(e) {
			var index = $(this).index();
			var delta = position - index;

			cansliding = true;
			position = index;
			prevPosition = position;

			for (var i = index - 1; i >= 0; i--) {
				// console.log( i );
				if (Anim[i]) {
					Anim[i](fp);
				}
				fp.pause();
			}

			Topslider.stop();
			Bottomslider.stop();

			if (index == 4) {
				// index = windowHeight < 700 ? 4 : 5;
				position = index;
				prevPosition = position;
			}

			if ( Math.abs(delta) == 1 ) {
				// console.log(  );
				showSlide(index, true, delta < 0);
			} else {
				$main.animate({opacity: 0}, Anim.dur.item, function() {
					showSlide(index, false, delta < 0);
					$main.animate({opacity: 1}, Anim.dur.item);
				});
			}
		});

		// $(".slider-skip").on("click", function() {
		// 	Topslider.showSlide(0);
		// 	Topslider.stop();
		// 	cansliding = true;
		// 	position = 2;
		// 	prevPosition = 2;
		// 	showSlide(2, true, true);
		// });

		addVendorEventListener(_main, "transitionEnd", function(e) {
			removeCSSValue(_main, "transition");
		}, false);
	});

})( jQuery, window );
